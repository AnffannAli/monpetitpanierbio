<?php
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', 'root');
    $requeterole = $bdd->prepare('SELECT *, role.nom as namerole FROM utilisateur INNER JOIN role ON role.id = utilisateur.id_role');
    $requeterole->execute();
    $reqRole = $requeterole->fetchAll();
    if(!empty($_POST['ident']) && !empty($_POST['mdp'])){
        foreach ($reqRole as $key => $value) {
            if($_POST['ident'] == $value['identifiant'] && $_POST['mdp'] == $value['mdp']){
                if($value['namerole'] == 'SuperAdmin'){
                    header('Location: pages/home.php');
                }
                else{
                    header('Location: index.php');
                }
            }
            else{
                header('Location: index.php');
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Connexion</title>
</head>
<body>
    <div class="container border">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4 ml-3">
                <img class="imgLogo" src="assets/monpanierbiologo2.svg">
            </div>
            <div class="col-lg-4"></div>
        </div> 
        <form method="POST" action="index.php">
            <div class="form-group">
                <label for="exampleInputEmail1">Identifiant :</label>
                <input type="text" class="form-control"  name="ident">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Mot de passe :</label>
                <input type="password" class="form-control" name="mdp" >
            </div>
            <button  type="submit" class="connecter">Se connecter</button>
        </form>
    </div>    
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</html>